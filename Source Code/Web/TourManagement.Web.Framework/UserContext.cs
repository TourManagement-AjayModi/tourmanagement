﻿using HRIMS.Core;
using HRIMS.Entities;

namespace HRIMS.Web.Framework
{
    public class UserContext : IUserContext
    {
        public User GetCurrentUser { get; private set; }
        public int UserId { get; private set; }
        public string UserName { get; private set; }
        public bool IsSuperAdmin { get; private set; }
    }
}
