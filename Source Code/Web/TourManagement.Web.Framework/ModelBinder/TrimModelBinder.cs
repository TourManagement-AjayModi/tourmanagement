﻿using System.ComponentModel;
using System.Web.Mvc;

namespace HRIMS.Web.Framework.ModelBinder
{
    public class TrimModelBinder : DefaultModelBinder
    {
        #region Protected Members

        protected override void SetProperty(ControllerContext controllerContext, ModelBindingContext bindingContext,
        PropertyDescriptor propertyDescriptor, object value)
        {
            if (propertyDescriptor.PropertyType == typeof(string))
            {
                var stringValue = (string)value;
                if (!string.IsNullOrEmpty(stringValue))
                    stringValue = stringValue.Trim();

                value = stringValue;
            }
            base.SetProperty(controllerContext, bindingContext, propertyDescriptor, value);
        } 

        #endregion
    }
}
