﻿using System.Collections;
using System.Web.Mvc;
using System.Linq;

namespace HRIMS.Web.Framework.Helpers
{
    public static class ModelStateHelper
    {
        #region Public Members

        public static IEnumerable Errors(this ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {
                return modelState.ToDictionary(kvp => kvp.Key,
                    kvp => kvp.Value.Errors.Select(e => SetMessageFormat(e.ErrorMessage)).ToArray()).Where(m => m.Value.Any());
            }
            return null;
        } 

        #endregion

        #region Private Members

        private static string SetMessageFormat(string errorMessage)
        {
            return errorMessage;
        } 

        #endregion
    }
}
