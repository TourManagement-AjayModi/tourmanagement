﻿using System.Web;

namespace HRIMS.Web.Framework.Helpers
{
    public static class SessionHelper
    {
        #region Public Members

        /// <summary>
        /// Sets the specified value to save.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session">The session.</param>
        /// <param name="valueToSave">The value to save.</param>
        public static void Set<T>(this HttpSessionStateBase session, T valueToSave)
        {
            Set(session, typeof(T).FullName, valueToSave);
        }

        /// <summary>
        /// Sets the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session">The session.</param>
        /// <param name="key">The key.</param>
        /// <param name="valueToSave">The value to save.</param>
        public static void Set<T>(this HttpSessionStateBase session, string key, T valueToSave)
        {
            session[key] = valueToSave;
        }

        /// <summary>
        /// Gets the specified session.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session">The session.</param>
        /// <returns></returns>
        public static T Get<T>(this HttpSessionStateBase session)
        {
            return Get<T>(session, typeof(T).FullName);
        }

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session">The session.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static T Get<T>(this HttpSessionStateBase session, string key)
        {
            object value = session[key];
            if (value == null)
                return default(T);
            return (T)value;
        } 

        #endregion
    }
}
