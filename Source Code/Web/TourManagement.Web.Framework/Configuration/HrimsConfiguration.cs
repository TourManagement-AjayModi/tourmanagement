﻿using System.Configuration;
using HRIMS.Core.Configuration;

namespace HRIMS.Web.Framework.Configuration
{
    /// <summary>
    /// A class to get configuration settings from AppSettings section.
    /// </summary>
    /// <seealso cref="HRIMS.Core.Configuration.IConfiguration" />
    public class HrimsConfiguration : IConfiguration
    {
        /// <summary>
        /// Gets the SMTP server.
        /// </summary>
        /// <value>
        /// The SMTP server.
        /// </value>
        public string SmtpServer
        {
            get { return ConfigurationManager.AppSettings["SmtpServer"]; }
        }

        /// <summary>
        /// Gets the name of the SMTP user.
        /// </summary>
        /// <value>
        /// The name of the SMTP user.
        /// </value>
        public string SmtpUserName
        {
            get { return ConfigurationManager.AppSettings["SmtpUserName"]; }
        }

        /// <summary>
        /// Gets the SMTP password.
        /// </summary>
        /// <value>
        /// The SMTP password.
        /// </value>
        public string SmtpPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpPassword"];
            }
        }

        /// <summary>
        /// Gets the SMTP default credentials.
        /// </summary>
        /// <value>
        /// The SMTP default credentials.
        /// </value>
        public string SmtpDefaultCredentials
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpDefaultCredentials"];
            }
        }

        /// <summary>
        /// Gets the SMTP port.
        /// </summary>
        /// <value>
        /// The SMTP port.
        /// </value>
        public string SmtpPort
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpPort"];
            }
        }
    }
}
