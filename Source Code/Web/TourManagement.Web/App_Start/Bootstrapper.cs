﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using HRIMS.Core.Caching;
using HRIMS.Data.Infrastructure;
using HRIMS.Data.Repositories;
using HRIMS.Services;
using HRIMS.Web.Framework.Configuration;
using HRIMS.Web.Mappings;
using HRIMS.Modules.KRA.Web;


namespace HRIMS.Web
{
    public static class Bootstrapper
    {
        public static void Run()
        {
            SetAutofacContainer();

            //Configure AutoMapper
            AutoMapperConfiguration.Configure();
        }

        private static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            
            // web
            builder.RegisterModule(new AutofacWebTypesModule());
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // register modules
            builder.RegisterModule(new AutofacForKRA());

            // data
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof (EntityBaseRepository<>))
                .As(typeof (IEntityBaseRepository<>))
                .InstancePerLifetimeScope();
            
            // core
            builder.RegisterType<MemoryCacheManager>().As<ICacheManager>().InstancePerLifetimeScope();
            builder.RegisterType<HrimsConfiguration>().As<HRIMS.Core.Configuration.IConfiguration>().InstancePerLifetimeScope();

            // service. 
            // will register all interface and services by Naming Convetion. No need to register individual services now
            builder.RegisterAssemblyTypes(typeof(EmployeeService).Assembly)
              .Where(t => t.Name.EndsWith("Service"))
              .AsImplementedInterfaces().InstancePerLifetimeScope();


            builder.RegisterFilterProvider();
            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }
    }
}