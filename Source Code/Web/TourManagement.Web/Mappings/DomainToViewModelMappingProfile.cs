﻿using AutoMapper;

namespace HRIMS.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        #region Public Members

        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        } 

        #endregion

        #region Protected Members

        protected override void Configure()
        {
            throw new System.NotImplementedException();
        } 

        #endregion
    }
}