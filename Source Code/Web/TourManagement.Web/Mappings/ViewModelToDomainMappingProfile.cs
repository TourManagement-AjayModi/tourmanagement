﻿using AutoMapper;

namespace HRIMS.Web.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        #region Public Members

        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        } 

        #endregion

        #region Protected Members

        protected override void Configure()
        {
            throw new System.NotImplementedException();
        } 

        #endregion
    }
}