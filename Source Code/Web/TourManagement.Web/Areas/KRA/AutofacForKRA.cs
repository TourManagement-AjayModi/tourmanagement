﻿using Autofac;
using Autofac.Integration.Mvc;
using HRIMS.Modules.KRA.Web.Mapping;

namespace HRIMS.Modules.KRA.Web
{
    public class AutofacForKRA : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            //This will register any Controllers that are in this assembly
            builder.RegisterControllers(ThisAssembly);

            //Register any other components that relate to this particular Project
            KRAAutoMapperConfiguration.Configure();
        }
    }
}