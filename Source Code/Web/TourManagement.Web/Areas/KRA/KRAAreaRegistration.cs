﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace HRIMS.Modules.KRA.Web
{
    public class KRAAreaRegistration : AreaRegistration
    {
        #region Public Members

        public override string AreaName
        {
            get { return "KRA"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            RegisterRoutes(context);
            RegisterBundles(BundleTable.Bundles);
        } 

        #endregion

        #region Private Members

        private void RegisterBundles(BundleCollection bundles)
        {
            // Register Javascripts

            // Register css
        }

        private void RegisterRoutes(AreaRegistrationContext context)
        {
            context.MapRoute(
              name: "KRA_default",
              url: "KRA/{controller}/{action}/{id}",
              defaults: new { controller = "Test", action = "Index", id = UrlParameter.Optional },
              namespaces: new string[] { "HRIMS.Modules.KRA.Web.Controllers" });
        } 

        #endregion
    }
}