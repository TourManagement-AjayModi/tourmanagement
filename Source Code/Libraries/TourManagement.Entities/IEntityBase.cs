﻿namespace HRIMS.Entities
{
    public interface IEntityBase
    {
         int Id { get; set; }
    }
}
