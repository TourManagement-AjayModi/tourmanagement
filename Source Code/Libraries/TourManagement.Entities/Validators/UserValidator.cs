﻿using FluentValidation;

namespace HRIMS.Entities.Validators
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(c => c.FirstName).NotEmpty().WithMessage(ValidationMessages.Required);
            RuleFor(c => c.LastName).NotEmpty().WithMessage(ValidationMessages.Required);
            RuleFor(c => c.DateOfBirth).NotEmpty().WithMessage(ValidationMessages.Required);
            RuleFor(c => c.JoiningDate).NotEmpty().WithMessage(ValidationMessages.Required);
            RuleFor(c => c.Phone).Matches(RegExValidators.PhoneValidator).WithMessage(ValidationMessages.InValid);
            RuleFor(c => c.Email).Matches(RegExValidators.EmailValidator).WithMessage(ValidationMessages.InValid);
        }
    }
}
