﻿namespace HRIMS.Entities
{
    public static class RegExValidators
    {
        // Regular Expressions
        public const string EmailValidator = "(?:[\\w\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\`\\{\\|\\}\\~]+\\.)*[\\w\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\`\\{\\|\\}\\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\\-](?!\\.)){0,61}[a-zA-Z0-9]?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\\[(?:(?:[01]?\\d{1,2}|2[0-4]\\d|25[0-5])\\.){3}(?:[01]?\\d{1,2}|2[0-4]\\d|25[0-5])\\]))$";
        public const string PhoneValidator = @"^([0-9\-\(\)\+\s]+)$";
        public const string AlphaValidator = @"^([a-zA-Z_\s]+)$";
        public const string AlphaNumericValidator = @"^([a-zA-Z0-9 ]+)$";
        public const string NumericValidator = "^(0|[1-9][0-9]*)$";
    }
}
