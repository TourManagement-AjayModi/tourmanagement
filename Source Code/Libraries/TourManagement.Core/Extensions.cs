﻿namespace HRIMS.Core
{
    public static class Extensions
    {
        /// <summary>
        /// Determines whether [is null or default].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static bool IsNullOrDefault<T>(this T? value) where T : struct
        {
            return default(T).Equals(value.GetValueOrDefault());
        }
    }
}
