﻿using System;
using System.Runtime.Serialization;

namespace HRIMS.Core
{
    [Serializable]
    public class HrimsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the Exception class.
        /// </summary>
        public HrimsException()
        {

        }

        /// <summary>
        /// Initializes a new instance of the Exception class with specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public HrimsException(string message)
            : base(message)
        {

        }

        /// <summary>
        /// Initializes a new instance of the Exception class with specified error message.
        /// </summary>
        /// <param name="messageForamt">The message foramt.</param>
        /// <param name="args">The arguments.</param>
        public HrimsException(string messageForamt, params object[] args)
            : base(string.Format(messageForamt, args))
        {

        }

        /// <summary>
        /// Initializes a new instance of the Exception class with serialized data.
        /// </summary>
        /// <param name="info">The SerializationInfo that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The SerializationContext that contains contextual information about the source or destination.</param>
        public HrimsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        /// <summary>
        /// Initializes a new instance of the Exception class with specified error message and inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public HrimsException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
