﻿using HRIMS.Entities;

namespace HRIMS.Core
{
    public interface IUserContext
    {
        User GetCurrentUser { get; }
        int UserId { get; }
        string UserName { get; }
        bool IsSuperAdmin { get; }
    }
}
