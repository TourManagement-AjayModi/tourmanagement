﻿
namespace HRIMS.Core.Configuration
{
    /// <summary>
    /// Represents a HRIMS configurations. All configuration settings must be retrieve via IConfiguration
    /// </summary>
    public interface IConfiguration
    {
        string SmtpServer { get;  }
        string SmtpUserName { get;  }
        string SmtpPassword { get;  }
        string SmtpDefaultCredentials { get;  }
        string SmtpPort { get;  }
    }
}
