﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using HRIMS.Data.Infrastructure;
using HRIMS.Entities;

namespace HRIMS.Data.Repositories
{
    public class EntityBaseRepository<T> : IEntityBaseRepository<T> where T : class , IEntityBase, new()
    {
        #region Declarations

        private HrimsContext _dbContext; 

        #endregion

        #region Constructors

        public EntityBaseRepository(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
        }
        
        #endregion

        #region Public Memebers

        public IDbFactory DbFactory { get; private set; }

        public IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbContext.Set<T>();
            return includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public IQueryable<T> All
        {
            get { return GetAll(); }
        }

        public IQueryable<T> GetAll()
        {
            return DbContext.Set<T>();
        }

        public T GetSingle(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _dbContext.Set<T>().Where(predicate);
        }

        public void Add(T entity)
        {
            DbContext.Entry(entity);
            DbContext.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        public void Edit(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            dbEntityEntry.State = EntityState.Modified;
        }
        
        #endregion

        #region Protected Members

        protected HrimsContext DbContext
        {
            get { return _dbContext ?? (_dbContext = DbFactory.Init()); }
        } 
        #endregion
    }
}
