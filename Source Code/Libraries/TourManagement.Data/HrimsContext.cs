﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using HRIMS.Core;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Text;

namespace HRIMS.Data
{
    public class HrimsContext : DbContext
    {
        public HrimsContext()
            : base("HRIMSContext")
        {
            Database.SetInitializer<HrimsContext>(null);
        }

        #region Entity Sets

        public new IDbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }

        #endregion

        /// <summary>
        /// Commits this instance.
        /// </summary>
        public virtual void Commit()
        {
            try
            {
                base.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessage = new StringBuilder();
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage.Append(validationError.PropertyName + " : " + validationError.ErrorMessage);

                        // TODO: log the error in database or text file
                        //Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }

                throw new HrimsException(errorMessage.ToString());
            }
        }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();


            // will add all mapping classes in modle configurations
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                                                                          .Where(type => !String.IsNullOrEmpty(type.Namespace))
                                                                          .Where(type => type.BaseType != null && type.BaseType.IsGenericType && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }
        }
    }
}
