﻿using System.Data.Entity.ModelConfiguration;
using HRIMS.Entities;

namespace HRIMS.Data.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            HasKey(k => k.Id);
            ToTable("Users");
        }
    }
}
