﻿using System.Linq;
using HRIMS.Data.Repositories;
using HRIMS.Entities;

namespace HRIMS.Data.Extensions
{
    public static class UserExtensions
    {
        #region Public Members

        /// <summary>
        /// Check if user exists or not using specified email address
        /// </summary>
        /// <param name="userRepository">The employee repository.</param>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        public static bool IsUserExists(this IEntityBaseRepository<User> userRepository, string email)
        {
            return userRepository.GetAll().Any(c => c.Email.ToLower() == email.ToLower());
        }

        /// <summary>
        /// Gets the full name of the user.
        /// </summary>
        /// <param name="userRepository">The employee repository.</param>
        /// <param name="userId">The employee identifier.</param>
        /// <returns></returns>
        public static string GetUserFullName(this IEntityBaseRepository<User> userRepository, int userId)
        {
            var user = userRepository.GetSingle(userId);
            if (user == null)
                return string.Empty;

            return user.FirstName + " " + user.LastName;
        } 

        #endregion
    }
}
