﻿namespace HRIMS.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
