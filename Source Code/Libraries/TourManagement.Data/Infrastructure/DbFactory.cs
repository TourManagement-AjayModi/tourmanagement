﻿namespace HRIMS.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private HrimsContext _dbContext;
        public HrimsContext Init()
        {
            return _dbContext ?? (_dbContext = new HrimsContext());
        }

        public void Dispose()
        {
            if (_dbContext != null)
                _dbContext.Dispose();
        }
    }
}
