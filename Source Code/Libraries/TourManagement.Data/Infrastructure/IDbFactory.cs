﻿using System;

namespace HRIMS.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        HrimsContext Init();
    }
}
