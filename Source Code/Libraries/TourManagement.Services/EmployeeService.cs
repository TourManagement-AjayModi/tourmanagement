﻿using HRIMS.Data.Repositories;
using HRIMS.Entities;
using HRIMS.Services.Abstract;

namespace HRIMS.Services
{
    public class EmployeeService : IEmployeeService
    {
        #region Declarations

        private readonly IEntityBaseRepository<User> _employeeRepository; 

        #endregion

        #region Constructors

        public EmployeeService(IEntityBaseRepository<User> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
 
        #endregion
    }
}
